<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class ContratoController extends Controller
{
    
    public function buscar(Request $request) {
        if($request->has(['tipo', 'valor'])) {
            return \App\Models\Contrato::buscar($request->tipo, $request->valor);
        }
    }

    public function criar(Request $request) {
        $validacao = Validator::make($request->all(), \App\Models\Contrato::$regras);
        if($validacao->passes()) {
            return \App\Models\Contrato::criar($request);
        }else {
            return ["error" => true, "message" => "Preencha todos os campos corretamente"];
        }
    }

    public function deletar(Request $request) {
        if($request->has('id')){
            if(\App\Models\Contrato::find($request->id)->delete() ) 
                return ["success" => true, "message" => "Contratado excluido com sucesso"];
            else
                return ["error" => true, "message" => "Houve um erro inesperado"];
        }else
            return ["error" => true, "message" => "Por favor, selecione um contrato primeiro"];
    }
    
    public function alterar(Request $request) {
        $validacao = Validator::make($request->all(), \App\Models\Contrato::$regrasAlterar);
        if($validacao->passes()) {
            if($contrato = \App\Models\Contrato::find($request->id)){
                $data = $request->only(["cnpj", "razao_social", "nome_fantasia", "email", "status"]);
                $data["cnpj"] = str_replace([".","/","-"], "", $data["cnpj"]);
                if($contrato->update($data)) {
                    return ["success" => true, "message" => "Contratado alterado com sucesso"];
                }else
                    return ["error" => true, "message" => "Houve um erro desconhecido"];
            }else
                return ["error" => true, "message" => "Houve um erro desconhecido"];

        }else {
            return ["error" => true, "message" => "Preencha todos os campos corretamente"];
        }
    }

}
