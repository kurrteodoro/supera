<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class UnidadeController extends Controller
{

    public function buscar(Request $request) {
        if($request->has(['tipo', 'valor'])) {
            return \App\Models\Unidade::buscar($request->tipo, $request->valor);
        }
    }

    public function criar(Request $request) {
        $validacao = Validator::make($request->all(), \App\Models\Unidade::$regras);
        if($validacao->passes()) {
            return \App\Models\Unidade::criar($request);
        }else {
            return ["error" => true, "message" => "Preencha todos os campos corretamente"];
        }
    }

    public function deletar(Request $request) {
        if($request->has('id')){
            if(\App\Models\Unidade::find($request->id)->delete() ) 
                return ["success" => true, "message" => "Unidade excluida com sucesso"];
            else
                return ["error" => true, "message" => "Houve um erro inesperado"];
        }else
            return ["error" => true, "message" => "Por favor, selecione uma unidade primeiro"];
    }

    public function alterar(Request $request) {
        $validacao = Validator::make($request->all(), \App\Models\Unidade::$regrasAlterar);
        if($validacao->passes()) {
            if($unidade = \App\Models\Unidade::find($request->id)){
                $data = $request->only(["nome_fantasia", "email", "estado", "status"]);
                if($unidade->update($data)) {
                    return ["success" => true, "message" => "Unidade alterada com sucesso"];
                }else
                    return ["error" => true, "message" => "Houve um erro desconhecido"];
            }else
                return ["error" => true, "message" => "Houve um erro desconhecido"];

        }else {
            return ["error" => true, "message" => "Preencha todos os campos corretamente"];
        }
    }
}
