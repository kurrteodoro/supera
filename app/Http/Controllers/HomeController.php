<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $dados = array();
        $dados['contrato'] = \App\Models\Contrato::count('id');
        $dados['unidade'] = \App\Models\Unidade::count('id');
        $dados['usuario'] = \App\Models\Usuario::count('id');
        return view('dashboard', ['dados' => $dados]);
    }
}
