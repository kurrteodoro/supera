<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class UsuarioController extends Controller
{
    
    public function buscar(Request $request) {
        if($request->has(['tipo', 'valor'])) {
            return \App\Models\Usuario::buscar($request->tipo, $request->valor);
        }
    }

    public function criar(Request $request) {
        $validacao = Validator::make($request->all(), \App\Models\Usuario::$regras);
        if($validacao->passes()) {
            return \App\Models\Usuario::criar($request);
        }else {
            return ["error" => true, "message" => "Preencha todos os campos corretamente"];
        }
    }

    public function deletar(Request $request) {
        if($request->has('id')){
            if(\App\Models\Usuario::find($request->id)->delete() ) 
                return ["success" => true, "message" => "Usuário excluido com sucesso"];
            else
                return ["error" => true, "message" => "Houve um erro inesperado"];
        }else
            return ["error" => true, "message" => "Por favor, selecione um usuário primeiro"];
    }

}
