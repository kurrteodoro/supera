<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{

    protected $guarded = ['id'];

    public static $regras = [
        "cnpj" => "required|min:18|max:18",
        "razao_social" => "required|min:1|max:50",
        "nome_fantasia" => "required|min:1|max:50",
        "email" => "required|min:5|max:50",
        "status" => "required|min:1|max:1",
        "logomarca" => "required",
    ];
    
    public static $regrasAlterar = [
        "cnpj" => "required|min:18|max:18",
        "razao_social" => "required|min:1|max:50",
        "nome_fantasia" => "required|min:1|max:50",
        "email" => "required|min:5|max:50",
        "status" => "required|min:1|max:1",
        "id" => "required",
    ];
    
    public function scopeBuscar($query, $tipo, $valor) {
        if($tipo == "cnpj")
            $valor = str_replace([".","/","-"], "", $valor);
        $contrato = $query->where($tipo, 'like', "%$valor%")->first();
        if($contrato)
            return response()->json($contrato);
        return response()->json(["status" => "error"]);
    }

    public function scopeCriar($query, $request) {
        $path = $request->file('logomarca')->storeAs(
            'logomarcas',
            time().".".$request->file('logomarca')->extension()
        );
        if($path) {
            $data = $request->except(['_token', 'logomarca']);
            $data["logomarca"] = $path;
            $data["cnpj"] = str_replace([".","/","-"], "", $data["cnpj"]);
            if( $query->create($data) )
                return ["success" => true, "message" => "Contrato cadastrado com sucesso"];
            else
                return ["error" => true, "message" => "houve um erro inesperado"];
        }else
            return ["error" => true, "message" => "houve um erro inesperado"];
    }

}
