<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unidade extends Model
{

    protected $guarded = ['id'];

    public static $regras = [
        "nome_fantasia" => "required|min:1|max:50",
        "email" => "required|min:5|max:50",
        "estado" => "required",
        "status" => "required|min:1|max:1",
        "logomarca" => "required",
    ];
    
    public static $regrasAlterar = [
        "nome_fantasia" => "required|min:1|max:50",
        "email" => "required|min:5|max:50",
        "estado" => "required",
        "status" => "required|min:1|max:1",
        "id" => "required",
    ];

    public function scopeBuscar($query, $tipo, $valor) {
        $unidade = $query->where($tipo, 'like', "%$valor%")->first();
        if($unidade)
            return response()->json($unidade);
        return response()->json(["status" => "error"]);
    }

    public function scopeCriar($query, $request) {
        $path = $request->file('logomarca')->storeAs(
            'logomarcas',
            time().".".$request->file('logomarca')->extension()
        );
        if($path) {
            $data = $request->except(['_token', 'logomarca']);
            $data["logomarca"] = $path;
            if( $query->create($data) )
                return ["success" => true, "message" => "Unidade cadastrada com sucesso"];
            else
                return ["error" => true, "message" => "houve um erro inesperado"];
        }else
            return ["error" => true, "message" => "houve um erro inesperado"];
    }
}
