<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $guarded = ['id'];

    public static $regras = [
        "nome_fantasia" => "required|min:1|max:50",
        "nome" => "required|min:5|max:50",
        "cpf" => "required|min:14|max:14",
    ];
    
    public static $regrasAlterar = [
        "nome_fantasia" => "required|min:1|max:50",
        "nome" => "required|min:5|max:50",
        "cpf" => "required|min:14|max:14",
        "id" => "required",
    ];

    public function scopeBuscar($query, $tipo, $valor) {
        if($tipo == "cpf")
            $valor = str_replace([".","-"], "", $valor);
        $usuario = $query->where($tipo, 'like', "%$valor%")->get();
        if($usuario->count() > 0)
            return response()->json($usuario);
        return response()->json(["status" => "error"]);
    }

    public function scopeCriar($query, $request) {
        $data = $request->except(['_token']);
        $data["cpf"] = str_replace([".","-"], "", $data["cpf"]);
        if( $query->create($data) )
            return ["success" => true, "message" => "Usuario cadastrado com sucesso"];
        else
            return ["error" => true, "message" => "houve um erro inesperado"];
    }
}
