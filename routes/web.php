<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

	// Contrato
	Route::get('buscar/contrato', 'ContratoController@buscar');
	Route::post('criar/contrato', 'ContratoController@criar');
	Route::get('deletar/contrato', 'ContratoController@deletar');
	Route::get('alterar/contrato', 'ContratoController@alterar');

	// Unidade
	Route::post('criar/unidade', 'UnidadeController@criar');
	Route::get('buscar/unidade', 'UnidadeController@buscar');
	Route::get('deletar/unidade', 'UnidadeController@deletar');
	Route::get('alterar/unidade', 'UnidadeController@alterar');
	
	// Usuário
	Route::post('criar/usuario', 'UsuarioController@criar');
	Route::get('buscar/usuario', 'UsuarioController@buscar');
	Route::get('deletar/usuario', 'UsuarioController@deletar');




});

