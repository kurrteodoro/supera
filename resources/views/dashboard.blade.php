@extends('layouts.app')

@section('content')
@include('layouts.headers.cards', $dados)

<div>

    <div class="container-fluid mt-5">
        <div class="row card">
            <div class="row p-5">
                <div>
                    <div class="box-opt btn-success" data-toggle="modal" data-target="#modalContrato">
                        <i class="ni ni-badge"></i>
                    </div>
                    <p class="text-center">Contrato</p>
                </div>
                <div>
                    <div class="box-opt btn-default" data-toggle="modal" data-target="#modalUnidade">
                        <i class="ni ni-building"></i>
                    </div>
                    <p class="text-center">Unidade</p>
                </div>
                <div>
                    <div class="box-opt btn-primary" data-toggle="modal" data-target="#modalAtestados">
                        <i class="ni ni-single-copy-04"></i>
                    </div>
                    <p class="text-center">Atestados</p>
                </div>
                <div>
                    <div class="box-opt btn-info" data-toggle="modal" data-target="#modalUsuario">
                        <i class="ni ni-single-02"></i>
                    </div>
                    <p class="text-center">Usuário</p>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>

    <div class="modal fade" id="modalContrato" tabindex="-1" role="dialog" aria-labelledby="modalContratoLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Contrato</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" id="formContrato">
                    @csrf
                    <div class="modal-body">
                        <div style="opacity: 0; display: none" class="erro alert alert-danger" role="alert">
                            <strong>Ops, </strong><span></span>
                        </div>
                        <div style="opacity: 0; display: none" class="sucesso alert alert-success" role="alert">
                            <strong>Oba, </strong><span></span>
                        </div>
                        <div class="input-group mb-3">
                            <input id="contratoCNPJ" required name="cnpj" type="text" class="form-control"
                                placeholder="CNPJ" aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button id="buscarCNPJ" class="btn btn-outline-primary" type="button"
                                    id="button-addon2"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input id="razaoSocialContrato" required name="razao_social" type="text"
                                class="form-control" placeholder="Razão social" aria-label="Recipient's username"
                                aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button id="buscarRazaoSocial" class="btn btn-outline-primary" type="button"
                                    id="button-addon2"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input id="nomeFantasiaContrato" required name="nome_fantasia" type="text"
                                class="form-control" placeholder="Nome fantasia" aria-label="Recipient's username"
                                aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button id="buscarNomeFantasia" class="btn btn-outline-primary" type="button"
                                    id="button-addon2"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input id="emailContrato" required name="email" type="text" class="form-control"
                                placeholder="E-mail" aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button id="buscarEmailContrato" class="btn btn-outline-primary" type="button"
                                    id="button-addon2"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                        <div>
                            <label for="customFileLang">Logomarca</label>
                            <input name="logomarca" type="file" class="custom-file-input" id="customFileLang" lang="en">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Status</label>
                            <select id="statusContrato" name="status" class="form-control"
                                id="exampleFormControlSelect1">
                                <option value="0">Ativo</option>
                                <option value="1">Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer row">
                        <button id="incluirContrato" type="button" class="mt-2 btn btn-success">Incluir</button>
                        <button id="alterarContrato" type="button" class="mt-2 btn btn-primary">Alterar</button>
                        <button id="excluirContrato" type="button" class="mt-2 btn btn-danger">Excluir</button>
                        <button id="limparContrato" type="reset" class="mt-2 btn btn-warning">Limpar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalUnidade" tabindex="-1" role="dialog" aria-labelledby="modalUnidadeLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Unidade</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" id="formUnidade">
                    @csrf
                    <div class="modal-body">
                        <div style="opacity: 0; display: none" class="erro alert alert-danger" role="alert">
                            <strong>Ops, </strong><span></span>
                        </div>
                        <div style="opacity: 0; display: none" class="sucesso alert alert-success" role="alert">
                            <strong>Oba, </strong><span></span>
                        </div>
                        <div class="input-group mb-3">
                            <input id="nomeFantasiaUnidade" required name="nome_fantasia" type="text"
                                class="form-control" placeholder="Nome fantasia" aria-label="Recipient's username"
                                aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button id="buscarNomeFantasiaUnidade" class="btn btn-outline-primary" type="button"
                                    id="button-addon2"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input id="emailUnidade" required name="email" type="text" class="form-control"
                                placeholder="E-mail" aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button id="buscarEmailUnidade" class="btn btn-outline-primary" type="button"
                                    id="button-addon2"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Estado</label>
                            <select id="estadoUnidade" name="estado" class="form-control">
                                <option value="AC">AC</option>
                                <option value="AL">AL</option>
                                <option value="AP">AP</option>
                                <option value="AM">AM</option>
                                <option value="BA">BA</option>
                                <option value="CE">CE</option>
                                <option value="ES">ES</option>
                                <option value="GO">GO</option>
                                <option value="MA">MA</option>
                                <option value="MT">MT</option>
                                <option value="MS">MS</option>
                                <option value="MG">MG</option>
                                <option value="PA">PA</option>
                                <option value="PB">PB</option>
                                <option value="PR">PR</option>
                                <option value="PE">PE</option>
                                <option value="PI">PI</option>
                                <option value="RJ">RJ</option>
                                <option value="RN">RN</option>
                                <option value="RS">RS</option>
                                <option value="RO">RO</option>
                                <option value="RR">RR</option>
                                <option value="SC">SC</option>
                                <option value="SP">SP</option>
                                <option value="SE">SE</option>
                                <option value="TO">TO</option>
                                <option value="DF">DF</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Status</label>
                            <select id="statusUnidade" name="status" class="form-control"
                                id="exampleFormControlSelect1">
                                <option value="0">Ativo</option>
                                <option value="1">Inativo</option>
                            </select>
                        </div>
                        <div>
                            <label for="customFileLang">Logomarca</label>
                            <input name="logomarca" type="file" class="custom-file-input" id="customFileLang" lang="en">
                        </div>
                    </div>
                    <div class="modal-footer row">
                        <button id="incluirUnidade" type="button" class="mt-2 btn btn-success">Incluir</button>
                        <button id="alterarUnidade" type="button" class="mt-2 btn btn-primary">Alterar</button>
                        <button id="excluirUnidade" type="button" class="mt-2 btn btn-danger">Excluir</button>
                        <button id="limparUnidade" type="reset" class="mt-2 btn btn-warning">Limpar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalAtestados" tabindex="-1" role="dialog" aria-labelledby="modalAtestadosLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Atestados</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" id="formAtestados">
                    @csrf
                    <p class="text-center ">ATENÇÃO, não compreendi muito bem essa parte, por isso fiz somente o layout
                    </p>
                    <div class="modal-body">
                        <div style="opacity: 0; display: none" class="erro alert alert-danger" role="alert">
                            <strong>Ops, </strong><span></span>
                        </div>
                        <div style="opacity: 0; display: none" class="sucesso alert alert-success" role="alert">
                            <strong>Oba, </strong><span></span>
                        </div>
                        <div class="input-group mb-3">
                            <input id="nomeFantasiaAtestados" required name="nome_fantasia" type="text"
                                class="form-control" placeholder="Nome fantasia" aria-label="Recipient's username"
                                aria-describedby="button-addon2">
                        </div>
                        <div class="input-group mb-3">
                            <input id="integracaoAtestados" required name="integracao" type="text" class="form-control"
                                placeholder="Integração" aria-label="Recipient's username"
                                aria-describedby="button-addon2">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Paciente</label>
                            <select id="" name="status" class="form-control" id="exampleFormControlSelect1">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Acompanhante</label>
                            <select id="" name="status" class="form-control" id="exampleFormControlSelect1">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Atestado</label>
                            <select id="" name="status" class="form-control" id="exampleFormControlSelect1">
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalUsuario" tabindex="-1" role="dialog" aria-labelledby="modalUsuarioLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Usuário</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" id="formUsuario">
                    @csrf
                    <div class="modal-body">
                        <div style="opacity: 0; display: none" class="erro alert alert-danger" role="alert">
                            <strong>Ops, </strong><span></span>
                        </div>
                        <div style="opacity: 0; display: none" class="sucesso alert alert-success" role="alert">
                            <strong>Oba, </strong><span></span>
                        </div>
                        <div class="input-group mb-3">
                            <input id="nomeFantasiaUsuario" required name="nome_fantasia" type="text"
                                class="form-control" placeholder="Nome fantasia" aria-label="Recipient's username"
                                aria-describedby="button-addon2">
                        </div>
                        <div class="input-group mb-3">
                            <input id="cpfUsuario" required name="cpf" type="text" class="form-control"
                                placeholder="CPF" aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button id="buscarCpfUsuario" class="btn btn-outline-primary" type="button"
                                    id="button-addon2"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input id="nomeUsuario" required name="nome" type="text" class="form-control"
                                placeholder="Nome" aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button id="buscarNomeUsuario" class="btn btn-outline-primary" type="button"
                                    id="button-addon2"><i class="fas fa-search"></i></button>
                            </div>
                        </div>

                        <div class="table-responsive">
                           <div style="max-height: 150px;">
                                <table class="table align-items-center table-cursor">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col" class="sort" data-sort="name">CPF</th>
                                            <th scope="col" class="sort" data-sort="budget">Usuário</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyUsuarios" class="list">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="modal-footer row">
                            <button id="incluirUsuario" type="button" class="mt-2 btn btn-success">Incluir</button>
                            <button id="excluirUsuario" type="button" class="mt-2 btn btn-danger">Excluir</button>
                            <button id="limparUsuario" type="reset" class="mt-2 btn btn-warning">Limpar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@push('js')
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
<script src="/js/jquery.mask.js?v=1.0"></script>
<script src="/js/home/index.js?v=1.0"></script>
@endpush
