
var contratoSelecionado = null;
var unidadeSelecionada = null;

$( document ).ready(function() {

    $('input[name=cnpj]').mask('99.999.999/9999-99', {reverse: true});
    $('input[name=cpf]').mask('999.999.999-99', {reverse: true});

    sistemaDeBusca();

    $("#incluirContrato").on('click', function() {
        contratoSelecionado = null;
        $("#formContrato").prop('action', '/criar/contrato');
        enviarForm( 
            $("#formContrato"),
            function(data) {
                if(data.success)
                    mostrarSucesso($("#formContrato").find('.sucesso'), data.message);
                else
                    mostrarErro($("#formContrato").find('.erro'), data.message);
            },
            null,
            "POST"
        );
    });
    
    $("#excluirContrato").on('click', function() {
        if(confirm('Confirmar exclusão ?')) {
            if(contratoSelecionado){
                $("#formContrato").prop('action', '/deletar/contrato');
                enviarForm(
                    $("#formContrato"),
                    function(data) {
                        if(data.success){
                            mostrarSucesso($("#formContrato").find('.sucesso'), data.message);
                            $("#limparContrato").click();
                        }
                        else
                            mostrarErro($("#formContrato").find('.erro'), data.message);
                    },
                    null,
                    "GET",
                    "id=" + contratoSelecionado
                );
            }else
                mostrarErro($("#formContrato").find('.erro'), "Selecione um contrato primeiro");
        }
    });
    
    $("#alterarContrato").on('click', function() {
        if(confirm('Confirmar alteração ?')){
            if(contratoSelecionado){
                $("#formContrato").prop('action', '/alterar/contrato');
                enviarForm(
                    $("#formContrato"),
                    function(data) {
                        if(data.success){
                            mostrarSucesso($("#formContrato").find('.sucesso'), data.message);
                        }
                        else
                            mostrarErro($("#formContrato").find('.erro'), data.message);
                    },
                    null,
                    "GET",
                    "id=" + contratoSelecionado
                );
            }else
                mostrarErro($("#formContrato").find('.erro'), "Selecione um contrato primeiro");
        }
    });

    $("#incluirUnidade").on('click', function() {
        unidadeSelecionada = null;
        $("#formUnidade").prop('action', '/criar/unidade');
        enviarForm( 
            $("#formUnidade"),
            function(data) {
                if(data.success)
                    mostrarSucesso($("#formUnidade").find('.sucesso'), data.message);
                else
                    mostrarErro($("#formUnidade").find('.erro'), data.message);
            },
            null,
            "POST"
        );
    });

    $("#excluirUnidade").on('click', function() {
        if(confirm('Confirmar exclusão ?')) {
            if(unidadeSelecionada){
                $("#formUnidade").prop('action', '/deletar/unidade');
                enviarForm(
                    $("#formUnidade"),
                    function(data) {
                        if(data.success){
                            mostrarSucesso($("#formUnidade").find('.sucesso'), data.message);
                            $("#limparUnidade").click();
                        }
                        else
                            mostrarErro($("#formUnidade").find('.erro'), data.message);
                    },
                    null,
                    "GET",
                    "id=" + unidadeSelecionada
                );
            }else
                mostrarErro($("#formUnidade").find('.erro'), "Selecione uma unidade primeiro");
        }
    });

    $("#alterarUnidade").on('click', function() {
        if(confirm('Confirmar alteração ?')){
            if(unidadeSelecionada){
                $("#formUnidade").prop('action', '/alterar/unidade');
                enviarForm(
                    $("#formUnidade"),
                    function(data) {
                        if(data.success){
                            mostrarSucesso($("#formUnidade").find('.sucesso'), data.message);
                        }
                        else
                            mostrarErro($("#formUnidade").find('.erro'), data.message);
                    },
                    null,
                    "GET",
                    "id=" + unidadeSelecionada
                );
            }else
                mostrarErro($("#formContrato").find('.erro'), "Selecione um contrato primeiro");
        }
    });

    $("#incluirUsuario").on('click', function() {
        unidadeSelecionada = null;
        $("#formUsuario").prop('action', '/criar/usuario');
        enviarForm( 
            $("#formUsuario"),
            function(data) {
                if(data.success)
                    mostrarSucesso($("#formUsuario").find('.sucesso'), data.message);
                else
                    mostrarErro($("#formUsuario").find('.erro'), data.message);
            },
            null,
            "POST"
        );
    });

    $("#excluirUsuario").on('click', function() {
        if(confirm('Confirmar exclusão ?')) {
            if($(".usuarioAtivado").length > 0){
                $("#formUsuario").prop('action', '/deletar/usuario');
                enviarForm(
                    $("#formUsuario"),
                    function(data) {
                        if(data.success){
                            mostrarSucesso($("#formUsuario").find('.sucesso'), data.message);
                            $("#limparUsuario").click();
                            $(".usuarioAtivado").remove();
                        }
                        else
                            mostrarErro($("#formUsuario").find('.erro'), data.message);
                    },
                    null,
                    "GET",
                    "id=" + $(".usuarioAtivado").data('usuario_id')
                );
            }else
                mostrarErro($("#formUsuario").find('.erro'), "Selecione um usuário primeiro");
        }
    });
  

});

function buscarContrato(tipo, valor) {
    $("#formContrato").prop('action', '/buscar/contrato');
    enviarForm( 
        $("#formContrato"),
        function(data) {
            if(data.status != "error"){
                mostrarSucesso($("#formContrato").find('.sucesso'), "Contrato encontrado com sucesso");
                $("#contratoCNPJ").val(data.cnpj);
                $("#razaoSocialContrato").val(data.razao_social);
                $("#nomeFantasiaContrato").val(data.nome_fantasia);
                $("#emailContrato").val(data.email);
                $("#statusContrato").val(data.status);
                contratoSelecionado = data.id;
            }else
                mostrarErro($("#formContrato").find('.erro'), "Nenhum contrato encontrado");
        },
        null,
        "GET",
        "tipo="+tipo+"&valor="+valor
    );
}

function buscarUnidade(tipo, valor) {
    $("#formUnidade").prop('action', '/buscar/unidade');
    enviarForm( 
        $("#formUnidade"),
        function(data) {
            if(data.status != "error"){
                mostrarSucesso($("#formUnidade").find('.sucesso'), "Unidade encontrada com sucesso");
                $("#nomeFantasiaUnidade").val(data.nome_fantasia);
                $("#emailUnidade").val(data.email);
                $("#estadoUnidade").val(data.estado);
                $("#statusUnidade").val(data.status);
                unidadeSelecionada = data.id;
            }else
                mostrarErro($("#formUnidade").find('.erro'), "Nenhuma unidade encontrado");
        },
        null,
        "GET",
        "tipo="+tipo+"&valor="+valor
    );
}

function selecionarUsuario(cpf, nome, nome_fantasia, id){
    $(".usuarioAtivado").removeClass('usuarioAtivado');
    $("#nomeFantasiaUsuario").val(nome_fantasia);
    $("#cpfUsuario").val(cpf);
    $("#nomeUsuario").val(nome);
    $('input[name=cpf]').unmask().mask('999.999.999-99', {reverse: true});
    $("#trUsuario"+id).addClass('usuarioAtivado');
}

function buscarUsuario(tipo, valor) {
    $("#formUsuario").prop('action', '/buscar/usuario');
    enviarForm( 
        $("#formUsuario"),
        function(data) {
            if(data.status != "error") {
                let html = '';
                data.forEach(function(element, key) {
                    html += `
                    <tr data-usuario_id="${element.id}" id="trUsuario${element.id}" onclick="selecionarUsuario('${element.cpf}', '${element.nome}', '${element.nome_fantasia}', '${element.id}')">
                        <td>${element.cpf}</td>
                        <td>${element.nome}</td>
                    </tr>
                    `;
                });
                $("#tbodyUsuarios").html(html);
                mostrarSucesso($("#formUsuario").find('.sucesso'), "Usuario(s) encontrado(s) com sucesso");
            }else{
                mostrarErro($("#formUsuario").find('.erro'), "Nenhum usuário encontrado");
            }
        },
        null,
        "GET",
        "tipo="+tipo+"&valor="+valor
    );
}

function enviarForm(form, callback=null, callbackErro=null, method="GET", data=null) {
    $.ajax({
        type: method,
        url: $(form).prop('action'),
        data: method == "GET" ? (data ? $(form).serialize() + "&"+data : $(form).serialize()) : new FormData(form[0]),
        cache: false,
        contentType: false,
        processData: false,
        success: function(data) {
            if(typeof callback == "function")
                callback(data);
        },
        error: function(data) {
            if(typeof callbackErro == "function")
                callbackErro(data);
        }
    });  
    esconderErros();
}

function mostrarErro(divErro, erro) {
    esconderSucessos();
    $(divErro).find('span').text(erro);
    $(divErro).show();
    $(divErro).animate({
        opacity: 1
    });
}

function esconderErros() {
    $(".erro").hide();
}

function mostrarSucesso(divSucesso, sucesso) {
    esconderErros();
    $(divSucesso).find('span').text(sucesso);
    $(divSucesso).show();
    $(divSucesso).animate({
        opacity: 1
    });
}

function esconderSucessos() {
    $(".sucesso").hide();
}

function sistemaDeBusca() {
    $("#buscarCNPJ").on('click', function() {
        buscarContrato('cnpj', $("#contratoCNPJ").val() );
    });
    $("#buscarRazaoSocial").on('click', function() {
        buscarContrato('razao_social', $("#razaoSocialContrato").val() );
    });
    $("#buscarNomeFantasia").on('click', function() {
        buscarContrato('nome_fantasia', $("#nomeFantasiaContrato").val() );
    });
    $("#buscarEmailContrato").on('click', function() {
        buscarContrato('email', $("#emailContrato").val() );
    });

    $("#buscarNomeFantasiaUnidade").on('click', function() {
        buscarUnidade('nome_fantasia', $("#nomeFantasiaUnidade").val() );
    });
    $("#buscarEmailUnidade").on('click', function() {
        buscarUnidade('email', $("#emailUnidade").val() );
    });
    
    $("#buscarCpfUsuario").on('click', function() {
        buscarUsuario('cpf', $("#cpfUsuario").val() );
    });
    $("#buscarNomeUsuario").on('click', function() {
        buscarUsuario('nome', $("#nomeUsuario").val() );
    });
}